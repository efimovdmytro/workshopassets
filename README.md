Workshop "Shaders for Artists"


![Shader Preview](/Preview/Shader_02.gif)


[Shader Graph Manual](https://docs.unity3d.com/Packages/com.unity.shadergraph@5.10/manual/index.html)

[Unity 2018.3.7](https://unity3d.com/get-unity/download/archive?_ga=2.234063190.950107331.1551704388-1572565529.1504081012)
